﻿using System.Collections.Generic;

namespace SimHubOBS
{
    /// <summary>
    /// Settings class, make sure it can be correctly serialized using JSON.net
    /// </summary>
    public class SimHubOBSSettings
    {
        public string IpAdresse { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public int UpdateRateOffset { get; set; }
        public bool MultiThreaded { get; set; }
        public List<CustomActionContainer> CustomActions { get; set; }
    }
}