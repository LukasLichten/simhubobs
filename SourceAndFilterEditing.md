SetSourceSettings:
SetSourceSettings only requires you to pass the properties you want to change, others will remain the same (overlay mode).
You Edit a Source, so changing the settings will change it in all Scenes it is present
Passing Properties that don't exist won't do anything, but the will appear then in the list of optionals for this Source

Known Issues with SetSourceSettings:
- Display Capture can only be changed when setting to obtuse monitor_id, changing the monitor value does nothing but flash the capture
- Color Source (Text, etc) color is a obscure integer number
- Text Font and Size are passed in a JsonObject as font
- ImageSlideShow files is a JsonArray with JsonObjects in it
- ImageSlideShow will reset to the first object when settings are changed
- It is not possible to control (pause, skip, etc) ImageSlideShow And Media Source during playback
- WindowSource/GameCapture window can be set, however the value is not convenient

Known Working:
- Browser Source url
- GameCapture capture_mode and window
- Text Sources text and opacity
- Image/Media Sources change file path
- Image Slide shows slide time

SetSourceFilterSettings:  
Works same as SetSourceSettings, except for Filters.  
  
Known Issues with SetFilterSettings:  
- Any Color is set through an obtuse value
- Certain Settings are missing from the drop down, but can be added manually, or show up if the value is not default:
  - Alpha LUT: image_path
  - Crop/Pad: left/right/top/bottom
  - Image Mask/Blend: image_path, strech
  - Render Delay: delay_ms
  - Scroll: limit_cx/limit_cy (Limit Width, Limit Hight), speed_x/speed_y (Horizontal/Vertical Speed)
  
Known Working:
- All OBS default filters