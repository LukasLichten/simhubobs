﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using OBSWebsocketDotNet;
using System.Threading;
using System.Collections.Generic;
using Lichten.SimHubHelper;
using SimHubOBS.Helper;
using System.Threading.Tasks;
using OBSWebsocketDotNet.Types;
using OBSWebsocketDotNet.Types.Events;
using OBSWebsocketDotNet.Communication;
using System.Windows.Media;

namespace SimHubOBS
{



    [PluginDescription("Connects OBS with Simhub, to allow reading data and controlling it")]
    [PluginAuthor("Lukas Lichten")]
    [PluginName("SimHub OBS Connector")]
    public class SimHubOBS : IPlugin, IDataPlugin, IWPFSettingsV2
    {
        public const string EXEC_TIME = "ExecutionTimeInMs";

        /// <summary>
        /// This serves for other Software to access the functions inside of this plugin.
        /// </summary>
        public static SimHubOBS Instance;

        /// <summary>
        /// Gets the left menu icon. Icon must be 24x24 and compatible with black and white display.
        /// </summary>
        public ImageSource PictureIcon => this.ToIcon(Properties.Resources.icon24);

        /// <summary>
        /// Gets a short plugin title to show in left menu. Return null if you want to use the title as defined in PluginName attribute.
        /// </summary>
        public string LeftMenuTitle => "SimHubOBS";

        public SimHubOBSSettings Settings { private set; get; }

        public OBSWebsocket OBSWebsocket { private set; get; }

        public PluginManager PluginManager { get; set; }

        public SimHubOBSCustomActions CustomActionManager { get; private set; }

        public List<IDataEvent> DataEvents { get; private set; }

        /// <summary>
        /// Name of all potential OBS main audio sources. Use GetSpecialSources on the OBSWebsocket to get those with actual devices attached.
        /// This does not cover audio coming from other sources, such as media
        /// </summary>
        public string[] MainAudioSources { get { return audioSourcesNames != null ? (string[]) audioSourcesNames.Clone() : new string[0]; } }

        internal Action<string> UpdateUIAlertBox { set; get; }

        internal int SkipCounter = 0;
        
        private IAsyncResult Task;

        private DateTime LastUpdate = DateTime.MinValue;

        private string[] audioSourcesNames;

        /// <summary>
        /// Called one time per game data update, contains all normalized game data, 
        /// raw data are intentionnally "hidden" under a generic object type (A plugin SHOULD NOT USE IT)
        /// 
        /// This method is on the critical path, it must execute as fast as possible and avoid throwing any error
        /// 
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            //This is to run at a lower update rate, and reduce load on the update thread
            if (SkipCounter < Settings.UpdateRateOffset)
            {
                SkipCounter++;
                return;
            }
            
            SkipCounter = 0;
            try
            {
                if (!Settings.MultiThreaded) //No Multithreading, normal execution
                {
                    Update();
                }
                else //Seperate Update Thread
                {
                    //Checking on the previous update thread
                    if (Task != null)
                    {
                        if (!Task.IsCompleted)
                        {
                            //Abort function
                            if ((DateTime.UtcNow - LastUpdate).TotalSeconds > 10)
                            {
                                //TODO actually terminating the task, right now we just let it run
                                Task = null;
                            }
                            else
                            {
                                if (!OBSWebsocket.IsConnected)
                                {
                                    return; //The Websocket has a 2s timeout, which means multiple updates will try to fire while it is still waiting to finish off, just skipping without error
                                }

                                SimHub.Logging.Current.Error("SimHubOBS: Update Skipped as the previous async update has not finished. Try turning off Update Thread if this occurrs too regularly");
                                return; //Skips this update, as the previous update has not finished yet
                            }
                        }

                        Task = null;
                    }

                    Action up = Update;
                    Task = up.BeginInvoke(null, null);
                }
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("SimHubOBS: Update failed due to exception: "+ex.Message);
            }
        }

        /// <summary>
        /// Called at plugin manager stop, close/dispose anything needed here ! 
        /// Plugins are rebuilt at game change
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            Instance = null;

            // Save settings
            this.SaveSettings();

            //Disconnect Websocket
            OBSWebsocket.Disconnect();
        }

        public void SaveSettings() {
            this.SaveCommonSettings("SimHubOBS", Settings);
        }

        /// <summary>
        /// Returns the settings control, return null if no settings control is required
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            return new ShObsSettingsControl(this);
        }

        /// <summary>
        /// Called once after plugins startup
        /// Plugins are rebuilt at game change
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            Instance = this;

            PluginManager = pluginManager;

            SimHubOBSSettings defaultValues = new SimHubOBSSettings()
            { IpAdresse = "127.0.0.1", Port = 4455, Password = "", UpdateRateOffset = 8, MultiThreaded = true, CustomActions = new List<CustomActionContainer>() };

            // Load settings
            Settings = this.ReadCommonSettings<SimHubOBSSettings>("SimHubOBS", () => defaultValues);

            audioSourcesNames = new string[]{ "Desktop Audio", "Desktop Audio 2", "Mic/Aux", "Mic/Aux 2", "Mic/Aux 3", "Mic/Aux 4" };


            OBSWebsocket = new OBSWebsocket();

            //Processing functions
            OBSWebsocket.Disconnected += OBSWebsocket_Disconnected;
            OBSWebsocket.ExitStarted += OBSWebsocket_ExitStarted;
            OBSWebsocket.Connected += OBSWebsocket_Connected;

            OBSWebsocket.InputActiveStateChanged += Update_InputActiveStateChanged;
            OBSWebsocket.InputVolumeChanged += Update_InputVolumeChanged;
            OBSWebsocket.InputMuteStateChanged += Update_InputMuteStateChanged;
            OBSWebsocket.InputVolumeMeters += Update_InputVolumeMeters;
            OBSWebsocket.CurrentProgramSceneChanged += Update_CurrentProgramSceneChanged;
            OBSWebsocket.SceneNameChanged += Update_SceneNameChanged;
            OBSWebsocket.CurrentPreviewSceneChanged += Update_CurrentPreviewSceneChanged;
            OBSWebsocket.StudioModeStateChanged += Update_StudioModeStateChanged;
            OBSWebsocket.CurrentSceneCollectionChanged += Update_CurrentSceneCollectionChanged;
            OBSWebsocket.CurrentProfileChanged += Update_CurrentProfileChanged;
            OBSWebsocket.ProfileListChanged += Update_ListChanged;
            OBSWebsocket.SceneCollectionListChanged += Update_ListChanged;
            OBSWebsocket.CurrentSceneTransitionChanged += Update_CurrentSceneTransitionChanged;
            OBSWebsocket.CurrentSceneTransitionDurationChanged += Update_CurrentSceneTransitionDurationChanged;
            OBSWebsocket.ReplayBufferStateChanged += Update_ReplayBufferStateChanged;
            OBSWebsocket.VirtualcamStateChanged += Update_VirtualcamStateChanged;

            //Helper Properties
            pluginManager.AddProperty("GameWindowOpen", this.GetType(), true.GetType(), null);
            pluginManager.AddProperty("GameProcessRunning", this.GetType(), true.GetType(), null);

            //Init of Properties
            pluginManager.AddProperty("Connected", this.GetType(), true.GetType(), null);

            pluginManager.AddProperty("CurrentProfile", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("CurrentScene", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("CurrentSceneCollection", this.GetType(), "".GetType(), null);

            pluginManager.AddProperty("AverageFrameTime", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("FPS", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("CpuUsage", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("RamUsage", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("FreeDiskSpace", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("EncodingSkippedFrames", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("EncodingTotalFrames", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("OutputDroppedFrames", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("OutputTotalFrames", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("RenderMissedFrames", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("RenderTotalFrames", this.GetType(), double.PositiveInfinity.GetType(), null);
            pluginManager.AddProperty("StreamTimecode", this.GetType(), TimeSpan.Zero.GetType(), null);
            pluginManager.AddProperty("RecordingTimecode", this.GetType(), TimeSpan.Zero.GetType(), null);

            pluginManager.AddProperty("IsRecording", this.GetType(), true.GetType(), null);
            pluginManager.AddProperty("IsRecordingPaused", this.GetType(), true.GetType(), null);
            pluginManager.AddProperty("IsStreaming", this.GetType(), true.GetType(), null);
            pluginManager.AddProperty("IsReplayBufferRunning", this.GetType(), true.GetType(), null);
            pluginManager.AddProperty("IsVirtualCameraOn", this.GetType(), true.GetType(), null);

            pluginManager.AddProperty("StreamingService", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("RecordingFolder", this.GetType(), "".GetType(), null);

            pluginManager.AddProperty("IsInStudioMode", this.GetType(), true.GetType(), null);
            pluginManager.AddProperty("CurrentTransition", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("CurrentTransitionDuration", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("StudioModePreviewScene", this.GetType(), "".GetType(), null);

            pluginManager.AddProperty("OBS-Studio_Version", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("OBS-Websocket_Version", this.GetType(), "".GetType(), null);
            pluginManager.AddProperty("OBS-Remote_Version", this.GetType(), "".GetType(), null);

            //Init of Actions
            pluginManager.AddAction("RecordingStart", this.GetType(), new ActionHelper(OBSWebsocket.StartRecord).TriggerAction);
            pluginManager.AddAction("RecordingStop", this.GetType(), new ActionHelper(() => OBSWebsocket.StopRecord()).TriggerAction);
            pluginManager.AddAction("RecordingToggle", this.GetType(), new ActionHelper(OBSWebsocket.ToggleRecord).TriggerAction);
            pluginManager.AddAction("StreamingStart", this.GetType(), new ActionHelper(OBSWebsocket.StartStream).TriggerAction);
            pluginManager.AddAction("StreamingStop", this.GetType(), new ActionHelper(OBSWebsocket.StopStream).TriggerAction);
            pluginManager.AddAction("StreamingToggle", this.GetType(), new ActionHelper(() => OBSWebsocket.ToggleStream()).TriggerAction);
            pluginManager.AddAction("ReplayBufferStart", this.GetType(), new ActionHelper(OBSWebsocket.StartReplayBuffer).TriggerAction);
            pluginManager.AddAction("ReplayBufferStop", this.GetType(), new ActionHelper(OBSWebsocket.StopReplayBuffer).TriggerAction);
            pluginManager.AddAction("ReplayBufferToggle", this.GetType(), new ActionHelper(OBSWebsocket.ToggleReplayBuffer).TriggerAction);

            pluginManager.AddAction("ReplayBufferSave", this.GetType(), new ActionHelper(OBSWebsocket.SaveReplayBuffer).TriggerAction);
            pluginManager.AddAction("RecordingPause", this.GetType(), new ActionHelper(OBSWebsocket.PauseRecord).TriggerAction);
            pluginManager.AddAction("RecordingResume", this.GetType(), new ActionHelper(OBSWebsocket.ResumeRecord).TriggerAction);

            pluginManager.AddAction("VirtualCameraStart", this.GetType(), new ActionHelper(OBSWebsocket.StartVirtualCam).TriggerAction);
            pluginManager.AddAction("VirtualCameraStop", this.GetType(), new ActionHelper(OBSWebsocket.StopVirtualCam).TriggerAction);
            pluginManager.AddAction("VirtualCameraToggle", this.GetType(), new ActionHelper(() => OBSWebsocket.ToggleVirtualCam()).TriggerAction);


            ListActionHelper sceneListAction = new ListActionHelper(OBSWebsocket.SetCurrentProgramScene, new SceneHelper(OBSWebsocket));
            pluginManager.AddAction("SceneNext", this.GetType(), sceneListAction.TriggerNextAction);
            pluginManager.AddAction("ScenePrevious", this.GetType(), sceneListAction.TriggerPreviousAction);
            ListActionHelper sceneCollectionListAction = new ListActionHelper(OBSWebsocket.SetCurrentSceneCollection,
                new SimpleListHelper(OBSWebsocket.GetCurrentSceneCollection, OBSWebsocket.GetSceneCollectionList));
            pluginManager.AddAction("SceneCollectionNext", this.GetType(), sceneCollectionListAction.TriggerNextAction);
            pluginManager.AddAction("SceneCollectionPrevious", this.GetType(), sceneCollectionListAction.TriggerPreviousAction);
            ListActionHelper profileListAction = new ListActionHelper(OBSWebsocket.SetCurrentProfile,
                new ProfileHelper(OBSWebsocket));
            pluginManager.AddAction("ProfileNext", this.GetType(), profileListAction.TriggerNextAction);
            pluginManager.AddAction("ProfilePrevious", this.GetType(), profileListAction.TriggerPreviousAction);
            ListActionHelper previewSceneListAction = new ListActionHelper(OBSWebsocket.SetCurrentPreviewScene, new SceneHelper(OBSWebsocket, true));
            pluginManager.AddAction("PreviewSceneNext", this.GetType(), previewSceneListAction.TriggerNextAction);
            pluginManager.AddAction("PreviewScenePrevious", this.GetType(), previewSceneListAction.TriggerPreviousAction);
            var transHelper = new TransitionHelper(OBSWebsocket);
            ListActionHelper transitionAction = new ListActionHelper(OBSWebsocket.SetCurrentSceneTransition, transHelper);
            pluginManager.AddAction("TransitionNext", this.GetType(), transitionAction.TriggerNextAction);
            pluginManager.AddAction("TransitionPrevious", this.GetType(), transitionAction.TriggerPreviousAction);

            IterationActionHelper transitionDurationAction = new IterationActionHelper(OBSWebsocket.SetCurrentSceneTransitionDuration, transHelper.GetDuration, 50);
            pluginManager.AddAction("TransitionDurationIncrease", this.GetType(), transitionDurationAction.TriggerIncrease);
            pluginManager.AddAction("TransitionDurationDecrease", this.GetType(), transitionDurationAction.TriggerDecrease);


            pluginManager.AddAction("StudioModeToggle", this.GetType(), new ActionHelper(() => OBSWebsocket.SetStudioModeEnabled(!OBSWebsocket.GetStudioModeEnabled())).TriggerAction);
            pluginManager.AddAction("StudioModeEnable", this.GetType(), new ActionHelper(() => OBSWebsocket.SetStudioModeEnabled(true)).TriggerAction);
            pluginManager.AddAction("StudioModeDisable", this.GetType(), new ActionHelper(() => OBSWebsocket.SetStudioModeEnabled(false)).TriggerAction);
            pluginManager.AddAction("StudioModeTransitionToProgram", this.GetType(), new ActionHelper(OBSWebsocket.TriggerStudioModeTransition).TriggerAction);
            

            //Init of Events
            OBSWebsocket.Connected += new OBSEventHelper<object>("OBSConnected", this).RegisterOBSEvent().EventHandle;
            OBSWebsocket.Disconnected += new OBSEventHelper<ObsDisconnectionInfo>("OBSDisconnected", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.ExitStarted += new OBSEventHelper<object>("OBSExited", this).RegisterOBSEvent().EventHandle;

            OBSWebsocket.CurrentProfileChanged += new OBSEventHelper<CurrentProfileChangedEventArgs>("ProfileChange", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.CurrentProgramSceneChanged += new OBSEventHelper<ProgramSceneChangedEventArgs>("SceneChange", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.CurrentSceneCollectionChanged += new OBSEventHelper<CurrentSceneCollectionChangedEventArgs>("SceneCollectionChange", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.CurrentSceneTransitionChanged += new OBSEventHelper<CurrentSceneTransitionChangedEventArgs>("TransitionChanged", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.CurrentSceneTransitionDurationChanged += new OBSEventHelper<CurrentSceneTransitionDurationChangedEventArgs>("TransitionDurationChanged", this).RegisterOBSEvent().WebsocketEventHandle;

            OBSWebsocket.RecordStateChanged += new OBSEventHelper<RecordStateChanged>("Recording", this).RegisterOutputStateEventRec().RecordStateCallback;
            OBSWebsocket.StreamStateChanged += new OBSEventHelper<StreamStateChangedEventArgs>("Streaming", this).RegisterOutputStateEvent().StreamStateCallback;
            OBSWebsocket.ReplayBufferStateChanged += new OBSEventHelper<ReplayBufferStateChangedEventArgs>("ReplayBuffer", this).RegisterOutputStateEvent().ReplayStateCallback;
            OBSWebsocket.VirtualcamStateChanged += new OBSEventHelper<VirtualcamStateChangedEventArgs>("VirtualCamera", this).RegisterOutputStateEvent().VirtualCameraStateCallback;

            OBSWebsocket.ProfileListChanged += new OBSEventHelper<ProfileListChangedEventArgs>("ProfileListChanged", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.SceneCollectionListChanged += new OBSEventHelper<SceneCollectionListChangedEventArgs>("SceneCollectionListChanged", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.SceneListChanged += new OBSEventHelper<SceneListChangedEventArgs>("SceneListChanged", this).RegisterOBSEvent().WebsocketEventHandle;
            //OBSWebsocket.TransitionListChanged += new OBSEventHelper("TransitionListChanged", this).RegisterOBSEvent().EventHandle; //Seems to have been removed

            OBSWebsocket.SceneTransitionStarted += new OBSEventHelper<SceneTransitionStartedEventArgs>("TransitionBegin", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.SceneTransitionEnded += new OBSEventHelper<SceneTransitionEndedEventArgs>("TransitionEnd", this).RegisterOBSEvent().WebsocketEventHandle;

            OBSWebsocket.CurrentPreviewSceneChanged += new OBSEventHelper<CurrentPreviewSceneChangedEventArgs>("PreviewSceneChange", this).RegisterOBSEvent().WebsocketEventHandle;
            OBSWebsocket.StudioModeStateChanged += new OBSEventHelper<StudioModeStateChangedEventArgs>("StudioModeSwitched", this).RegisterOBSEvent().WebsocketEventHandle;


            //Custom Actions
            CustomActionManager = new SimHubOBSCustomActions(this);


            //Data Events
            DataEvents = new List<IDataEvent>
            {
                new DataOnOffEventHelper("GameWindow", this.GetType(), "SimHubOBS.GameWindowOpen", pluginManager, "Opened", "Closed"),
                new DataOnOffEventHelper("GameProcess", this.GetType(), "SimHubOBS.GameProcessRunning", pluginManager)
            };

            //Main Audio Source Properties, Events and Actions
            foreach (string item in audioSourcesNames)
            {
                string noSpace = item.Replace(" ", "");

                pluginManager.AddProperty("Audio_" + noSpace + "_IsActive" , this.GetType(), true.GetType(), "This is true even if the source is muted, this only defines if a device was defined and the source is not disabled (even if the device is not attached)");
                pluginManager.AddProperty("Audio_" + noSpace + "_IsMuted", this.GetType(), true.GetType(), null);
                pluginManager.AddProperty("Audio_" + noSpace + "_Volume", this.GetType(), double.MinValue.GetType(), "This is not the current output, this is the slider");
                //I would love to include a current volume read out, even if the update rate would not allow it to look good on a bar graph, just so you could check that OBS/Windows garbled up your sound settings, again...
                //And no, even the device ID optained through SourceSettings will be defined when the device is not found
                pluginManager.AddProperty("Audio_" + noSpace + "_VolumeMeter", this.GetType(), double.MinValue.GetType(), "Audio output level");

                pluginManager.AddAction("MuteToggle_" + noSpace, this.GetType(), new ValueActionHelper<string>(OBSWebsocket.ToggleInputMute, item).TriggerAction);
            }

            OBSWebsocket.InputMuteStateChanged += new AudioEventHelper(this).RegisterAudioEvents().MuteStateCallback;
        }

        private void Update_VirtualcamStateChanged(object sender, VirtualcamStateChangedEventArgs virtCam)
        {
            var outputState = virtCam.OutputState;
            PluginManager.SetPropertyValue("IsVirtualCameraOn", this.GetType(), outputState.IsActive);
        }

        #region Profile, Scene, Transition

        private void Update_ReplayBufferStateChanged(object sender, ReplayBufferStateChangedEventArgs e)
        {
            var outputState = e.OutputState;
            PluginManager.SetPropertyValue("IsReplayBufferRunning", this.GetType(), outputState.IsActive);
        }

        private void Update_CurrentSceneTransitionDurationChanged(object sender, CurrentSceneTransitionDurationChangedEventArgs transition)
        {
            PluginManager.SetPropertyValue("CurrentTransitionDuration", this.GetType(), transition.TransitionDuration);
        }

        private void Update_CurrentSceneTransitionChanged(object sender, CurrentSceneTransitionChangedEventArgs transition)
        {
            PluginManager.SetPropertyValue("CurrentTransition", this.GetType(), transition.TransitionName);
        }

        private void Update_ListChanged(object sender, SceneCollectionListChangedEventArgs sceneCollectionList)
        {
            //SceneCollection List has been updated, possibly the currently selected one has been renamed

            new Action(ForceUpdate).BeginInvoke(null, null); // Async call to avoid deadlock of websocket

            void ForceUpdate()
            {
                PluginManager.SetPropertyValue("CurrentSceneCollection", this.GetType(), OBSWebsocket.GetCurrentSceneCollection());
            }
        }

        private void Update_ListChanged(object sender, ProfileListChangedEventArgs profileList)
        {
            //ProfileList has been updated, possibly the currently selected one has been renamed

            new Action(ForceUpdate).BeginInvoke(null, null); // Async call to avoid deadlock of websocket

            void ForceUpdate()
            {
                PluginManager.SetPropertyValue("CurrentProfile", this.GetType(), OBSWebsocket.GetProfileList().CurrentProfileName);
            }
        }

        private void Update_CurrentProfileChanged(object sender, CurrentProfileChangedEventArgs profile)
        {
            PluginManager.SetPropertyValue("CurrentProfile", this.GetType(), profile.ProfileName);
        }

        private void Update_CurrentSceneCollectionChanged(object sender, CurrentSceneCollectionChangedEventArgs scene)
        {
            PluginManager.SetPropertyValue("CurrentSceneCollection", this.GetType(), scene.SceneCollectionName);
        }

        private void Update_StudioModeStateChanged(object sender, StudioModeStateChangedEventArgs studioModeState)
        {
            PluginManager.SetPropertyValue("IsInStudioMode", this.GetType(), studioModeState.StudioModeEnabled);

            if (studioModeState.StudioModeEnabled)
                new Action(UpdatePreviewScene).BeginInvoke(null, null); //Has to be done async, as otherwise we deadlock the Websocket
            else
                PluginManager.SetPropertyValue("StudioModePreviewScene", this.GetType(), null);
            

            void UpdatePreviewScene()
            {
                PluginManager.SetPropertyValue("StudioModePreviewScene", this.GetType(), OBSWebsocket.GetCurrentPreviewScene());
            }
        }

        private void Update_CurrentPreviewSceneChanged(object sender, CurrentPreviewSceneChangedEventArgs scene)
        {
            PluginManager.SetPropertyValue("StudioModePreviewScene", this.GetType(), scene.SceneName);
        }

        private void Update_SceneNameChanged(object sender, SceneNameChangedEventArgs scene)
        {
            string curr = PluginManager.GetPropertyValue("SimHubOBS.CurrentScene") as string;
            string currPre = PluginManager.GetPropertyValue("SimHubOBS.StudioModePreviewScene") as string;

            if (scene.OldSceneName == curr)
                PluginManager.SetPropertyValue("CurrentScene", this.GetType(), scene.SceneName);
            if (scene.OldSceneName == currPre)
                PluginManager.SetPropertyValue("StudioModePreviewScene", this.GetType(), scene.SceneName);
        }

        private void Update_CurrentProgramSceneChanged(object sender, ProgramSceneChangedEventArgs newScene)
        {
            PluginManager.SetPropertyValue("CurrentScene", this.GetType(), newScene.SceneName);
        }

        #endregion

        #region Audio Input Processing

        private string CheckIfSpecialInput(string inputName)
        {
            foreach (var item in audioSourcesNames)
            {
                if (item == inputName)
                    return item.Replace(" ", "");

            }

            return null;
        }

        private void Update_InputActiveStateChanged(object sender, InputActiveStateChangedEventArgs inputState)
        {
            string noSpace = CheckIfSpecialInput(inputState.InputName);
            if (noSpace == null)
                return;


            PluginManager.SetPropertyValue("Audio_" + noSpace + "_IsActive", this.GetType(), inputState.VideoActive);
        }

        private void Update_InputMuteStateChanged(object sender, InputMuteStateChangedEventArgs inputState)
        {
            string noSpace = CheckIfSpecialInput(inputState.InputName);
            if (noSpace == null)
                return;

            PluginManager.SetPropertyValue("Audio_" + noSpace + "_IsMuted", this.GetType(), inputState.InputMuted);
        }

        private void Update_InputVolumeChanged(object sender, InputVolumeChangedEventArgs inputVolume)
        {
            var volume = inputVolume.Volume;
            string noSpace = CheckIfSpecialInput(volume.InputName);
            if (noSpace == null)
                return;

            PluginManager.SetPropertyValue("Audio_" + noSpace + "_Volume", this.GetType(), volume.InputVolumeMul);
        }

        private void Update_InputVolumeMeters(object sender, InputVolumeMetersEventArgs inputs)
        {
            foreach (var meter in inputs.InputMeters)
            {
                string noSpace = CheckIfSpecialInput(meter.InputName);

                if (noSpace != null)
                {
                    double output = 0;

                    foreach (var item in meter.InputLevelsMul)
                    {
                        if (item == null || item.Length == 0)
                        {
                            output = 0;
                        }
                        else if (item.Length == 1)
                        {
                            output += item[0];
                        }
                        else if (item.Length >= 2)
                        {
                            output += item[1];
                        }
                    }

                    output /= meter.InputLevelsMul.Length;

                    PluginManager.SetPropertyValue("Audio_" + noSpace + "_VolumeMeter", this.GetType(), output);
                }
            }
        }

        #endregion

        private bool stopReidentifyToDeath = false;

        private void OBSWebsocket_Connected(object sender, EventArgs e)
        {
            //SkipCounter = Settings.UpdateRateOffset;

            if (!stopReidentifyToDeath)
                Task = new Action(InitialUpdate).BeginInvoke(null, null); //Has to be done in Parallel
        }

        private void InitialUpdate()
        {
            //Setting up additional event listeners
            stopReidentifyToDeath = true;
            OBSWebsocket.ReIdentify(new EventSubscription[] { EventSubscription.All, EventSubscription.InputVolumeMeters, EventSubscription.InputActiveStateChanged });

            //Setting static data
            var version = OBSWebsocket.GetVersion();
            PluginManager.SetPropertyValue("OBS-Studio_Version", this.GetType(), version.OBSStudioVersion);
            PluginManager.SetPropertyValue("OBS-Websocket_Version", this.GetType(), version.PluginVersion);
            PluginManager.SetPropertyValue("OBS-Remote_Version", this.GetType(), version.Version);

            //Scenes, Transitions, Profiles
            PluginManager.SetPropertyValue("CurrentScene", this.GetType(), OBSWebsocket.GetCurrentProgramScene());
            bool studioMode = OBSWebsocket.GetStudioModeEnabled();
            PluginManager.SetPropertyValue("IsInStudioMode", this.GetType(), studioMode);
            if (studioMode)
                PluginManager.SetPropertyValue("StudioModePreviewScene", this.GetType(), OBSWebsocket.GetCurrentPreviewScene()); //PreviewScene throws an except if StudioMode is off
            else
                PluginManager.SetPropertyValue("StudioModePreviewScene", this.GetType(), null);
            PluginManager.SetPropertyValue("CurrentProfile", this.GetType(), OBSWebsocket.GetProfileList().CurrentProfileName);
            PluginManager.SetPropertyValue("CurrentSceneCollection", this.GetType(), OBSWebsocket.GetCurrentSceneCollection());
            var currTrans = OBSWebsocket.GetCurrentSceneTransition();
            PluginManager.SetPropertyValue("CurrentTransition", this.GetType(), currTrans.Name);
            PluginManager.SetPropertyValue("CurrentTransitionDuration", this.GetType(), currTrans.Duration);
            try {
                PluginManager.SetPropertyValue("IsReplayBufferRunning", this.GetType(), OBSWebsocket.GetReplayBufferStatus());
            } catch {
                PluginManager.SetPropertyValue("IsReplayBufferRunning", this.GetType(), false);
            }
            

            //Audio
            var source = OBSWebsocket.GetSpecialInputs();

            foreach (string item in audioSourcesNames)
            {
                string noSpace = item.Replace(" ", "");

                bool audioActive = false;
                bool audioMuted = false;
                double audioVolume = double.NaN;

                foreach (var iter in source.Values)
                {
                    if (iter == item)
                    {
                        audioActive = true;
                        audioMuted = OBSWebsocket.GetInputMute(item);
                        audioVolume = OBSWebsocket.GetInputVolume(item).VolumeMul;
                        break;
                    }
                }

                PluginManager.SetPropertyValue("Audio_" + noSpace + "_IsActive", this.GetType(), audioActive);
                PluginManager.SetPropertyValue("Audio_" + noSpace + "_IsMuted", this.GetType(), audioMuted);
                PluginManager.SetPropertyValue("Audio_" + noSpace + "_Volume", this.GetType(), audioVolume);
                PluginManager.SetPropertyValue("Audio_" + noSpace + "_VolumeMeter", this.GetType(), double.NaN);
            }

            //Virtual Camera
            PluginManager.SetPropertyValue("IsVirtualCameraOn", this.GetType(), OBSWebsocket.GetVirtualCamStatus().IsActive);

            //Forcing a regular update after this
            SkipCounter = Settings.UpdateRateOffset;

            stopReidentifyToDeath = false;
        }

        private void OBSWebsocket_ExitStarted(object sender, EventArgs e)
        {
            //OBSWebsocket.Disconnect();
            SkipCounter = -600; // Prevents OBS from crashing while closing it
        }

        private void OBSWebsocket_Disconnected(object sender, OBSWebsocketDotNet.Communication.ObsDisconnectionInfo e)
        {
            if (e != null && e.ObsCloseCode == OBSWebsocketDotNet.Communication.ObsCloseCodes.AuthenticationFailed)
            {
                SimHub.Logging.Current.Error("SimHubOBS: Connection Error, "+e.DisconnectReason);
                SkipCounter = -300; //This will cause a 5s delay before a reconnect is attempted, at 60fps at least

                this.UpdateUIAlertBox.Invoke("SimHubOBS: Connection Error, "+e.DisconnectReason);
                
            }
        }

        //This method is called from DataUpdate, and runs all the data retrieval (and connecting)
        //It is a seperate Method so it can be called from a seperate Thread
        private void Update()
        {
            bool firstUpdate = LastUpdate == DateTime.MinValue && !OBSWebsocket.IsConnected;
            LastUpdate = DateTime.UtcNow;

            //Updating Game Process Running over here, as GetProcessName takes ~5ms to return
            var procEnum = PluginManager.GetProcesseNames().GetEnumerator();
            if (procEnum.MoveNext())
            {
                var pro = System.Diagnostics.Process.GetProcessesByName(procEnum.Current);

                if (pro.Length > 0)
                {
                    PluginManager.SetPropertyValue("GameProcessRunning", this.GetType(), true);
                    PluginManager.SetPropertyValue("GameWindowOpen", this.GetType(), pro[0].MainWindowHandle != IntPtr.Zero);
                }
                else
                {
                    PluginManager.SetPropertyValue("GameProcessRunning", this.GetType(), false);
                    PluginManager.SetPropertyValue("GameWindowOpen", this.GetType(), false);
                }
            }

            //When SimHub Auto switches games the events would fire, but the websocket is not yet connected, so we skip the first update
            if (!firstUpdate) 
            {
                foreach (var dataEvent in DataEvents)
                {
                    dataEvent.Update(PluginManager);
                }
            }

            //The Websocket Part
            PluginManager.SetPropertyValue("Connected", this.GetType(), OBSWebsocket.IsConnected);


            if (!OBSWebsocket.IsConnected) //Attempt connect
            {
                try
                {
                    OBSWebsocket.ConnectAsync("ws://" + Settings.IpAdresse + ":" + Settings.Port, Settings.Password);
                }
                catch (UriFormatException)
                {
                    SimHub.Logging.Current.Error("SimHubOBS: URI Format in Address invalid");
                    this.UpdateUIAlertBox.Invoke("URI Format in Address invalid");
                }
                //If this succeeds, it triggers the "OBSWebsocket_Connected" above, if it fails "OBSWebsocket_Disconnected" is triggered

                SkipCounter = -300;
                return;
            }

            try
            {
                var obsStats = OBSWebsocket.GetStats();
                var stream = OBSWebsocket.GetStreamStatus();
                var recStatus = OBSWebsocket.GetRecordStatus();

                var streamService = OBSWebsocket.GetStreamServiceSettings();
                var recordFolder = OBSWebsocket.GetRecordDirectory();

                PluginManager.SetPropertyValue("RecordingFolder", this.GetType(), recordFolder);

                PluginManager.SetPropertyValue("AverageFrameTime", this.GetType(), obsStats.AverageFrameTime);
                PluginManager.SetPropertyValue("FPS", this.GetType(), obsStats.FPS);
                PluginManager.SetPropertyValue("CpuUsage", this.GetType(), obsStats.CpuUsage);
                PluginManager.SetPropertyValue("RamUsage", this.GetType(), obsStats.MemoryUsage);
                PluginManager.SetPropertyValue("FreeDiskSpace", this.GetType(), obsStats.FreeDiskSpace);
                PluginManager.SetPropertyValue("EncodingSkippedFrames", this.GetType(), obsStats.OutputSkippedFrames);
                PluginManager.SetPropertyValue("EncodingTotalFrames", this.GetType(), obsStats.OutputTotalFrames);
                PluginManager.SetPropertyValue("RenderMissedFrames", this.GetType(), obsStats.RenderMissedFrames);
                PluginManager.SetPropertyValue("RenderTotalFrames", this.GetType(), obsStats.RenderTotalFrames);
                
                PluginManager.SetPropertyValue("OutputDroppedFrames", this.GetType(), stream.SkippedFrames);
                PluginManager.SetPropertyValue("OutputTotalFrames", this.GetType(), stream.TotalFrames);
                PluginManager.SetPropertyValue("IsStreaming", this.GetType(), stream.IsActive);
                PluginManager.SetPropertyValue("StreamTimecode", this.GetType(), TimeSpan.TryParse(stream.TimeCode, out TimeSpan streamCode) ? streamCode : TimeSpan.Zero);

                PluginManager.SetPropertyValue("IsRecording", this.GetType(), recStatus.IsRecording);
                PluginManager.SetPropertyValue("IsRecordingPaused", this.GetType(), recStatus.IsRecordingPaused);
                PluginManager.SetPropertyValue("RecordingTimecode", this.GetType(), TimeSpan.TryParse(recStatus.RecordTimecode, out TimeSpan recCode) ? recCode : TimeSpan.Zero);

                //Figuring out the streaming service
                string service = null;
                if (streamService.Settings.Server != null)
                {
                    string server = streamService.Settings.Server;
                    if (server == "auto" || server.Contains("live-video.net") || server.Contains("twitch.tv"))
                        service = "Twitch";
                    else if (server.Contains("youtube"))
                    {
                        service = "Youtube";

                        if (server.Contains("https"))
                            service += " - HLS";
                        else if (server.Contains("rtmps"))
                            service += " - RTMPS";
                        else
                            service += " - Legacy RTMP";
                    }
                    else if (server.Contains("restream"))
                        service = "Restream.io";
                    else if (server.Contains("vimeo"))
                        service = "Vimeo";
                    else if (server.Contains("pscp.tv"))
                        service = "Twitter";
                    else if (server.Contains("steamcontent"))
                        service = "Steam";
                    else if (server.Contains("facebook"))
                        service = "Facebook Live";
                    else if (server.Contains("onlyfans"))
                        service = "OnlyFans";
                    else if (server.Contains("global-contribute.live-video.net"))
                        service = "Kick.com";

                }

                if (service == null)
                    service = streamService.Type;

                PluginManager.SetPropertyValue("StreamingService", this.GetType(), service);
            }
            catch (NullReferenceException)
            {
                //Connection likely closed
            }
        }
    }
}