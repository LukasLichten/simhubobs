﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    public class CustomActionHelper
    {
        public Action<Dictionary<string, string>, PluginManager> Method { get; set; }
        public Dictionary<string,string> Propertys { get; set; }

        public CustomActionHelper() { }

        public CustomActionHelper(Action<Dictionary<string, string>, PluginManager> method, Dictionary<string, string> propertys)
        {
            this.Method = method;
            this.Propertys = propertys;
        }

        public void TriggerAction(PluginManager pluginManager, string data)
        {
            try
            {
                //Method.Invoke(Param[0].ToString());
                Method.BeginInvoke(Propertys, pluginManager, null, null);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }
    }
}
