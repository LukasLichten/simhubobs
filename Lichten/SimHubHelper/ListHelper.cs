﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{

    /// <summary>
    /// Used to get values for List Operations
    /// </summary>
    public abstract class ListHelper
    {
        public abstract string GetCurrent();
        public abstract string[] GetList();
    }
}
