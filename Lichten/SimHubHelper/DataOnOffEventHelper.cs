﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    public class DataOnOffEventHelper : DataEventHelper<bool>
    {
        private readonly string turnedOn = "Started";
        private readonly string turnedOff = "Stopped";

        public DataOnOffEventHelper(string eventName, Type plugin, string property, PluginManager pluginManager) : base(eventName, plugin, property)
        {
            RegisterEvent(pluginManager);
        }

        public DataOnOffEventHelper(string eventName, Type plugin, string property, PluginManager pluginManager, string turnOnSuffix, string turnOffSuffix) : base(eventName, plugin, property)
        {
            turnedOn = turnOnSuffix;
            turnedOff = turnOffSuffix;

            RegisterEvent(pluginManager);
        }

        private void RegisterEvent(PluginManager pluginManager)
        {
            pluginManager.AddEvent(EventName + turnedOn, Plugin);
            pluginManager.AddEvent(EventName + turnedOff, Plugin);
        }

        protected override void Evaluate(PluginManager pluginManager, bool value)
        {
            if (value != PreviousValue)
            {
                if (value)
                {
                    //TurnedOn
                    TriggerEvent(EventName + turnedOn, pluginManager);
                }
                else
                {
                    //TurnedOff
                    TriggerEvent(EventName + turnedOff, pluginManager);
                }
            }
        }
    }
}
