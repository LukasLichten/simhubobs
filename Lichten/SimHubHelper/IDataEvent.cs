﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    public interface IDataEvent
    {
        void Update(PluginManager pluginManager);
    }
}
