﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public interface ICustomActionFunction
    {
        void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager);
        
        string[] GetRequiredPropertys();

        string[] GetOptionalPropertys(Dictionary<string, string> propertys);

        string[] GetPossibleValues(Dictionary<string, string> propertys, string property);
    }
}
