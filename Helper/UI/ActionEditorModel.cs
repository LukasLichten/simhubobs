﻿using SimHub.Plugins.DataPlugins.RGBDriverCommon.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using static SimHub.Plugins.UI.SupportedGamePicker;

namespace SimHubOBS.Helper.UI
{
    public class ActionEditorModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<PropertyRow> _rows = new ObservableCollection<PropertyRow>();

        private CustomActionContainer _actionContainer;

        public CustomActionContainer ActionContainer
        {
            get => _actionContainer;
            set
            {
                _actionContainer = value;
                Name = value.Name;
                _rows.Clear();
                foreach (var val in value.Propertys)
                {
                    _rows.Add(new PropertyRow() { Name = val.Key, Value = val.Value});
                }
                Function = value.Function; // Also causes Rows to assign required
                //OnPropertyChanged("Rows");
            }
        }

        public ObservableCollection<PropertyRow> Rows
        {
            get => _rows;

            set
            {
                _rows = value;
                OnPropertyChanged();
            }
        }

        private string _name;

        public string Name {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        private string _function;

        public string Function {
            get => _function;
            set
            {
                _function = value;
                OnPropertyChanged();

                var func = Plugin.CustomActionManager.GetFunction(value);
                
                string[] _re = func?.GetRequiredPropertys();
                List<string> required = new List<string>();
                if (_re != null)
                {
                    required.AddRange(_re);
                }

                bool opt = func == null || func?.GetOptionalPropertys(this.BuildPropertys()) != null;

                // We try to find propertys for our required propertys, first we look for those with the same name, which should insure compatibility right of the bat
                int doneCount = 0;
                foreach (var row in Rows)
                {
                    if (required.Contains(row.Name))
                    {
                        row.NotRequired = false;
                        doneCount++;
                        required.Remove(row.Name);
                    }
                    else
                    {
                        row.NotRequired = true;
                    }
                }

                // Sorting the list so we have all the once we assigned already first, so we skip them with the doneCounter so we don't accidentally override
                // And because you can't simply sort a list in place in .NET I have to summon some cursed stuff in the following loop
                var sorted = Rows.OrderBy(i => i.NotRequired);

                // If we couldn't find propertys with the same name for the required we just coerce in numerical order
                for (int i = doneCount; i < (required.Count + doneCount); i++)
                {
                    var re = required[i - doneCount];
                    int index = i;
                    if (Rows.Count - 1 < i)
                    {
                        Rows.Add(new PropertyRow());
                    }
                    else
                    {
                        // We index in the sorted list as planned, but we need to edit on the Rows list
                        var test = sorted.ElementAt(i);
                        index = Rows.IndexOf(test);
                    }

                    // When updating Name the possible Values get updated, but this causes the Value in the textbox to be whipped if it isn't on the list
                    // To avoid updating every properties PossibleValues we set NotRequired to false 
                    Rows[index].NotRequired = true;
                    var val = Rows[index].Value;
                    

                    Rows[index].Name = re;

                    Rows[index].Value = val;

                    Rows[index].NotRequired = false;
                }

                // If there are no optional values we trim out the none optionals
                if (!opt)
                {
                    for (int i = 0; i < Rows.Count; i++)
                    {
                        var row = Rows[i];
                        if (row.NotRequired)
                        {
                            Rows.Remove(row);
                        }
                    }
                }

                OnPropertyChanged(nameof(HasOptional));
            }
        }

        public bool HasOptional {
            get
            {
                var func = Plugin.CustomActionManager.GetFunction(this.Function);
                return func == null || func?.GetOptionalPropertys(this.BuildPropertys()) != null;
            }
        
        }

        private bool _isEdit;

        public bool IsEdit
        {
            get => _isEdit;
            set
            {
                _isEdit = value;
                OnPropertyChanged();
            }
        }

        public string Title {
            get
            {
                return _isEdit ? "Edit" : "Create";
            }
            
        }

        public SimHubOBS Plugin { get; set; }

        public ObservableCollection<string> GetFunctionList {
            get
            {
                var list = new ObservableCollection<string>();

                if (Plugin == null)
                {
                    return list;
                }

                foreach (var item in Plugin.CustomActionManager.GetFunctionsList())
                {
                    list.Add(item);
                }

                return list;
            }
        }

        public string[] PossibleNames
        {
            get
            {
                var func = Plugin.CustomActionManager.GetFunction(this.Function);
                if (func == null)
                {
                    return new string[0];
                }

                return func.GetOptionalPropertys(this.BuildPropertys());
            }
        }

        public Dictionary<string, string> BuildPropertys()
        {
            Dictionary<string,string> properties = new Dictionary<string,string>();
            foreach (var item in this.Rows)
            {
                if (!string.IsNullOrEmpty(item.Name))
                    properties.Add(item.Name.Trim(), (item.Value ?? "").Trim());
            }
            return properties;
        }

        internal void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public class PropertyRow : INotifyPropertyChanged
        {
            private string _name;

            public string Name
            {
                get => _name;
                set
                {
                    _name = value;
                    OnPropertyChanged();
                }
            }

            private bool _required;

            public bool NotRequired
            {
                get => !_required;
                set
                {
                    _required = !value;
                    OnPropertyChanged();
                }
            }

            private string[] _possibleNames;

            public string[] PossibleNames
            {
                set
                {
                    var n = Name;
                    _possibleNames = value;
                    OnPropertyChanged();
                    Name = n;
                }

                get
                {
                    return _possibleNames;
                }
            }

            private string _value;

            public string Value
            {
                get => _value;
                set
                {
                    _value = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(PropButtonColor));
                }
            }

            private string[] _possibleValues;

            public string[] PossibleValues
            {
                set
                {
                    var v = Value;
                    _possibleValues = value;
                    OnPropertyChanged();
                    Value = v;
                }

                get
                {
                    return _possibleValues;
                }
            }

            public System.Windows.Media.Brush PropButtonColor
            {
                get
                {
                    return SimHubOBSCustomActions.IsValueAPropertysName(SimHubOBS.Instance.PluginManager, this.Value) ? System.Windows.Media.Brushes.Green : System.Windows.Media.Brushes.LightGray;
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
