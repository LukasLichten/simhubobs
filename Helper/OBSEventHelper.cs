﻿using Lichten.SimHubHelper;
using Newtonsoft.Json.Linq;
using OBSWebsocketDotNet;
using OBSWebsocketDotNet.Communication;
using OBSWebsocketDotNet.Types;
using OBSWebsocketDotNet.Types.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public class OBSEventHelper<T> : EventHelper
    {
        public OBSEventHelper(string eventName, SimHubOBS main) : base()
        {
            EventName = eventName;
            Plugin = main.GetType();
            PluginManager = main.PluginManager;
        }

        public OBSEventHelper<T> RegisterOBSEvent()
        {
            RegisterEvent();
            return this;
        }

        public OBSEventHelper<T> RegisterOutputStateEvent()
        {
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STARTED), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STARTING), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STOPPED), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STOPPING), Plugin);

            return this;
        }

        public OBSEventHelper<T> RegisterOutputStateEventRec()
        {
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STARTED), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STARTING), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STOPPED), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_STOPPING), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_PAUSED), Plugin);
            PluginManager.AddEvent(ConvertOutputStateString(OutputState.OBS_WEBSOCKET_OUTPUT_RESUMED), Plugin);

            return this;
        }

        public void OBS_OutputStateCallback(object sender, OutputStateChanged outputStateChanged)
        {
            TriggerEvent(ConvertOutputStateString(outputStateChanged.State));
        }

        private string ConvertOutputStateString(OutputState type)
        {
            switch (type) {
                case OutputState.OBS_WEBSOCKET_OUTPUT_STARTED:
                    return EventName + "Started";
                case OutputState.OBS_WEBSOCKET_OUTPUT_STARTING:
                    return EventName + "Starting";
                case OutputState.OBS_WEBSOCKET_OUTPUT_STOPPED:
                    return EventName + "Stopped";
                case OutputState.OBS_WEBSOCKET_OUTPUT_STOPPING:
                    return EventName + "Stopping";
                case OutputState.OBS_WEBSOCKET_OUTPUT_PAUSED:
                    return EventName + "Paused";
                case OutputState.OBS_WEBSOCKET_OUTPUT_RESUMED:
                    return EventName + "Resumed";
            }

            return EventName;
        }

        public void WebsocketEventHandle(object sender, T args) { EventHandle(sender, null); }

        public void RecordStateCallback(object sender, RecordStateChangedEventArgs args) { OBS_OutputStateCallback(sender, args.OutputState); }
        public void StreamStateCallback(object sender, StreamStateChangedEventArgs args) { OBS_OutputStateCallback(sender, args.OutputState); }
        public void ReplayStateCallback(object sender, ReplayBufferStateChangedEventArgs args) { OBS_OutputStateCallback(sender, args.OutputState); }
        public void VirtualCameraStateCallback(object sender, VirtualcamStateChangedEventArgs args) { OBS_OutputStateCallback(sender, args.OutputState); }
    }
}
