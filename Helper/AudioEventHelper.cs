﻿using Lichten.SimHubHelper;
using OBSWebsocketDotNet;
using OBSWebsocketDotNet.Types.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public class AudioEventHelper : EventHelper
    {
        private string[] AudioSources;

        public AudioEventHelper(SimHubOBS main) : base()
        {
            Plugin = main.GetType();
            PluginManager = main.PluginManager;
            AudioSources = main.MainAudioSources;
        }

        public AudioEventHelper RegisterAudioEvents()
        {
            foreach (var item in AudioSources)
            {
                string noSpace = item.Replace(" ", "");

                PluginManager.AddEvent("Audio_" + noSpace + "_Muted", Plugin);
                PluginManager.AddEvent("Audio_" + noSpace + "_Unmuted", Plugin);
            }
            return this;
        }

        public void MuteStateCallback(object sender, InputMuteStateChangedEventArgs inputMute)
        {
            bool isContained = false;

            foreach (var item in AudioSources)
            {
                if (inputMute.InputName == item)
                {
                    isContained = true;
                    break;
                }
            }

            if (isContained)
            {
                string suffix = inputMute.InputMuted ? "_Muted" : "_Unmuted";
                string noSpace = inputMute.InputName.Replace(" ", "");

                TriggerEvent("Audio_" + noSpace + suffix);
            }
        }
    }
}
