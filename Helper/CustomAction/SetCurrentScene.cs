﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetCurrentScene : ICustomActionFunction
    {
        protected SimHubOBS Plugin { get; set; }

        public SetCurrentScene(SimHubOBS plugin)
        {
            Plugin = plugin;
        }

        public string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            return null;
        }

        public string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            return new SceneHelper(Plugin.OBSWebsocket).GetList();
        }

        public string[] GetRequiredPropertys()
        {
            return new string[] { "SceneName" };
        }

        public virtual void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string sceneName= SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "SceneName");
            if (sceneName == null)
            {
                SimHub.Logging.Current.Error("Trying to change Scene with custom action failed, no SceneName Value defined");
                return;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return;
            }

            try
            {
                Plugin.OBSWebsocket.SetCurrentProgramScene(sceneName);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Scene with a custom action: " + ex.Message);
            }
}
    }
}
