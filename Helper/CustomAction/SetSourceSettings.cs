﻿using Newtonsoft.Json.Linq;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetSourceSettings : ASceneItemHelper
    {

        public SetSourceSettings(SimHubOBS plugin) : base(plugin, true) { }

        public override string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            string source = SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "SourceName");
            if (source == null || !Plugin.OBSWebsocket.IsConnected)
            {
                return new string[0];
            }

            List<string> optionals = new List<string>();

            try
            {
                var input = Plugin.OBSWebsocket.GetInputSettings(source);
                foreach (var item in input.Settings)
                {
                    if (!propertys.ContainsKey(item.Key))
                    {
                        optionals.Add(item.Key);
                    }
                }

                var defaultSettings = Plugin.OBSWebsocket.GetInputDefaultSettings(input.InputKind);


                foreach (var item in defaultSettings)
                {
                    if (!propertys.ContainsKey(item.Key) && !optionals.Contains(item.Key))
                    {
                        optionals.Add(item.Key);
                    }
                }

                return optionals.ToArray();
            }
            catch
            {
                return optionals.ToArray();
            }
        }

        public override string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            if (property == "SourceName")
            {
                return base.GetPossibleValues(propertys, property);
            }

            try
            {
                List<string> values = new List<string>();
                var input = Plugin.OBSWebsocket.GetInputSettings(SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "SourceName"));
                if (input.Settings.ContainsKey(property))
                {
                    values.Add(input.Settings[property].ToString());
                }

                var defaultSettings = Plugin.OBSWebsocket.GetInputDefaultSettings(input.InputKind);
                if (defaultSettings.ContainsKey(property))
                {
                    string val = defaultSettings[property].ToString();
                    if (!values.Contains(val))
                    {
                        values.Add(val);
                    }
                }

                return values.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public override void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string source = this.GetSource(propertys, pluginManager);

            var settings = new JObject();
            foreach (var property in propertys)
            {
                if (property.Key != "SourceName")
                {
                    JToken value = property.Value;
                    if (Int64.TryParse(property.Value, out long valInt))
                    {
                        value = valInt;
                    }
                    else if (Double.TryParse(property.Value, out double valDouble))
                    {
                        value = valDouble;
                    }
                    else if (Boolean.TryParse(property.Value, out bool valBoolean))
                    {
                        value = valBoolean;
                    }

                    settings.Add(property.Key, value);
                }
            }
            try
            {
                Plugin.OBSWebsocket.SetInputSettings(source, settings, overlay: true);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Source Settings with a custom action: " + ex.Message);
            }
            
        }


    }
}
