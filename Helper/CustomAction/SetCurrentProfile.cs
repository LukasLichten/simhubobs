﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetCurrentProfile : ICustomActionFunction
    {
        private SimHubOBS Plugin { get; set; }

        public SetCurrentProfile(SimHubOBS plugin)
        {
            Plugin = plugin;
        }

        public string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            return null;
        }

        public string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            return new ProfileHelper(Plugin.OBSWebsocket).GetList();
        }

        public string[] GetRequiredPropertys()
        {
            return new string[] { "ProfileName" };
        }

        public void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string profileName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "ProfileName");
            if (profileName == null)
            {
                SimHub.Logging.Current.Error("Trying to change Profile with custom action failed, no ProfileName Value defined");
                return;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return;
            }

            try
            {
                Plugin.OBSWebsocket.SetCurrentProfile(profileName);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Profile with a custom action: " + ex.Message);
            }
        }
    }
}
