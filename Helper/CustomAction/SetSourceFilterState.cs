﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetSourceFilterState : ASceneItemHelper
    {
        private Mode OperatingMode { get; set; }

        public SetSourceFilterState(SimHubOBS plugin, Mode mode) : base(plugin, true)
        {
            OperatingMode = mode;
        }

        public override string[] GetRequiredPropertys()
        {
            List<string> result = base.GetRequiredPropertys().ToList();
            result.Add("FilterName");
            return result.ToArray();
        }

        public override string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            return null;
        }

        public override string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            if (property == "FilterName")
            {
                String sourceName = this.GetSource(propertys, Plugin.PluginManager);

                if (sourceName == null)
                {
                    return null;
                }

                var list = Plugin.OBSWebsocket.GetSourceFilterList(sourceName);
                var result = new List<string>();
                foreach (var item in list)
                {
                    result.Add(item.Name);
                }

                return result.ToArray();
            }
            else
            {
                return base.GetPossibleValues(propertys, property);
            }
        }


        public override void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            String sourceName = this.GetSource(propertys, pluginManager);

            if (sourceName == null)
            {
                return;
            }

            String filterName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "FilterName");

            try
            {
                switch (OperatingMode)
                {
                    case Mode.Enable:
                        Plugin.OBSWebsocket.SetSourceFilterEnabled(sourceName, filterName, true);
                        break;
                    case Mode.Disable:
                        Plugin.OBSWebsocket.SetSourceFilterEnabled(sourceName, filterName, false);
                        break;
                    case Mode.Toggle:
                        bool state = Plugin.OBSWebsocket.GetSourceFilter(sourceName, filterName).IsEnabled;
                        Plugin.OBSWebsocket.SetSourceFilterEnabled(sourceName, filterName, !state);
                        break;
                }
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Source Filter State with a custom action: " + ex.Message);
            }
        }

        public enum Mode
        {
            Enable, Disable, Toggle
        }
    }
}
