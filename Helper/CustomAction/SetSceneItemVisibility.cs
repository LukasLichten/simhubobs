﻿using Lichten.SimHubHelper;
using OBSWebsocketDotNet;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.AxHost;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetSceneItemVisibility : ASceneItemHelper
    {
        private Mode OperatingMode { get; set; }

        public SetSceneItemVisibility(SimHubOBS plugin, Mode operatingMode) : base(plugin)
        {
            OperatingMode = operatingMode;
        }

        public override void MapAndRun(Dictionary<string, string> propertys, PluginManager  pluginManager)
        {
            SceneItem item = this.GetSceneItem(propertys, pluginManager);
            if (item == null)
            {
                return;
            }
            string scene = item.Scene;
            int id = item.Id;

            try
            {
                switch (OperatingMode)
                {
                    case Mode.Enable:
                        Plugin.OBSWebsocket.SetSceneItemEnabled(scene, id, true);
                        break;
                    case Mode.Disable:
                        Plugin.OBSWebsocket.SetSceneItemEnabled(scene, id, false);
                        break;
                    case Mode.Toggle:
                        bool state = Plugin.OBSWebsocket.GetSceneItemEnabled(scene, id);
                        Plugin.OBSWebsocket.SetSceneItemEnabled(scene, id, !state);
                        break;
                }
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Source Visibility with a custom action: " + ex.Message);
            }
        }

        public enum Mode
        {
            Enable, Disable, Toggle
        }
    }
}
