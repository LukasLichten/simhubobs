﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetPreviewScene : SetCurrentScene
    {
        public SetPreviewScene(SimHubOBS plugin) : base(plugin) {}

        public override void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string sceneName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "SceneName");
            if (sceneName == null)
            {
                SimHub.Logging.Current.Error("Trying to change PreviewScene with custom action failed, no SceneName Value defined");
                return;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return;
            }

            try
            {
                Plugin.OBSWebsocket.SetCurrentPreviewScene(sceneName);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change PreviewScene with a custom action: " + ex.Message);
            }
    
        }
    }
}
