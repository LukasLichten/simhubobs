﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetCurrentSceneCollection : ICustomActionFunction
    {
        private SimHubOBS Plugin { get; set; }

        public SetCurrentSceneCollection(SimHubOBS plugin)
        {
            Plugin = plugin;
        }

        public string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            return null;
        }

        public string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            return Plugin.OBSWebsocket.GetSceneCollectionList().ToArray();
        }

        public string[] GetRequiredPropertys()
        {
            return new string[] { "SceneCollectionName" };
        }

        public void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string sceneCollection = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "SceneCollectionName");
            if (sceneCollection == null)
            {
                SimHub.Logging.Current.Error("Trying to change SceneCollection with custom action failed, no SceneCollectionName Value defined");
                return;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return;
            }

            try
            {
                Plugin.OBSWebsocket.SetCurrentSceneCollection(sceneCollection);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Scene Collection with a custom action: " + ex.Message);
            }
}
    }
}
