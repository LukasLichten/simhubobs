﻿using Newtonsoft.Json.Linq;
using OBSWebsocketDotNet;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public abstract class ASceneItemHelper : ICustomActionFunction
    {
        protected SimHubOBS Plugin { get; set; }
        private bool NoScene { get; set; }

        public ASceneItemHelper(SimHubOBS plugin)
        {
            Plugin = plugin;
            NoScene = false;
        }

        public ASceneItemHelper(SimHubOBS plugin, bool noScene)
        {
            Plugin = plugin;
            NoScene = noScene;
        }

        public virtual string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            if (propertys.ContainsKey("SceneName") || NoScene)
            {
                return new string[0];
            }
            else
            {
                return new string[] { "SceneName" };
            }
        }

        public virtual string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            if (property == "SourceName")
            {
                string scene = SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "SceneName") ?? Plugin.OBSWebsocket.GetCurrentProgramScene();
                var list = Plugin.OBSWebsocket.GetSceneItemList(scene);
                string[] arr = new string[list.Count];

                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = list[i].SourceName;
                }

                return arr;
            }
            else if (property == "SceneName" && !NoScene)
            {
                return new SceneHelper(Plugin.OBSWebsocket).GetList();
            }

            return null;
        }

        public virtual string[] GetRequiredPropertys()
        {
            return new string[] { "SourceName" };
        }

        public abstract void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager);

        protected SceneItem GetSceneItem(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string sourceName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "SourceName");
            if (sourceName == null)
            {
                SimHub.Logging.Current.Error("Trying to change Source Visibility with custom action failed, no SourceName Value defined");
                return null;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
                return null;

            string scene = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "SceneName") ?? Plugin.OBSWebsocket.GetCurrentProgramScene();


            int id;
            try
            {
                id = Plugin.OBSWebsocket.GetSceneItemId(scene, sourceName, 0);
            }
            catch (ErrorResponseException)
            {
                SimHub.Logging.Current.Error("Trying to change Source Visibility with custom action failed, Source with name " + sourceName + " not found in scene " + scene);
                return null;
            }

            return new SceneItem() { Scene = scene, Id = id };
        }

        public String GetSource(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string sourceName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "SourceName");
            if (sourceName == null)
            {
                SimHub.Logging.Current.Error("Trying to change Source Visibility with custom action failed, no SourceName Value defined");
                return null;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
                return null;

            return sourceName;
        }

        protected class SceneItem
        {
            public string Scene;
            public int Id;
        }
    }
}
