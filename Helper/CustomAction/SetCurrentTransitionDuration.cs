﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetCurrentTransitionDuration : ICustomActionFunction
    {
        private SimHubOBS Plugin { get; set; }

        public SetCurrentTransitionDuration(SimHubOBS plugin)
        {
            Plugin = plugin;
        }

        public string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            return null;
        }

        public string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            return null;
        }

        public string[] GetRequiredPropertys()
        {
            return new string[] { "TransitionDuration" };
        }

        public virtual void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string transition = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "TransitionDuration");
            if (transition == null)
            {
                SimHub.Logging.Current.Error("Trying to change Transition Duration with custom action failed, no TransitionDuration Value defined");
                return;
            }

            if (!double.TryParse(transition, out double val))
            {
                SimHub.Logging.Current.Error("Trying to change Transition Duration with custom action failed, TransitionDuration Value was not a number");
                return;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return;
            }

            try
            {
                Plugin.OBSWebsocket.SetCurrentSceneTransitionDuration((int)val);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Transition Duration with a custom action: " + ex.Message);
            }
            
        }
    }
}
