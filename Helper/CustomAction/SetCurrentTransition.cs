﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetCurrentTransition : ICustomActionFunction
    {
        private SimHubOBS Plugin { get; set; }

        public SetCurrentTransition(SimHubOBS plugin)
        {
            Plugin = plugin;
        }

        public string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            return null;
        }

        public string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            return new TransitionHelper(Plugin.OBSWebsocket).GetList();
        }

        public string[] GetRequiredPropertys()
        {
            return new string[] { "TransitionName" };
        }

        public virtual void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string transition = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "TransitionName");
            if (transition == null)
            {
                SimHub.Logging.Current.Error("Trying to change Transition with custom action failed, no TransitionName Value defined");
                return;
            }

            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return;
            }

            try {
                Plugin.OBSWebsocket.SetCurrentSceneTransition(transition);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Transition with a custom action: " + ex.Message);
            }
}
    }
}
