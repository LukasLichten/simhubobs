﻿using Newtonsoft.Json.Linq;
using SimHub.Plugins;
using SimHub.Plugins.DataPlugins.ShakeItV3.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper.CustomAction
{
    public class SetSourceFilterSettings : ASceneItemHelper
    {

        public SetSourceFilterSettings(SimHubOBS plugin) : base(plugin, true) { }

        public override string[] GetRequiredPropertys()
        {
            List<string> result = base.GetRequiredPropertys().ToList();
            result.Add("FilterName");
            return result.ToArray();
        }

        public override string[] GetOptionalPropertys(Dictionary<string, string> propertys)
        {
            string source = SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "SourceName");
            string filterName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "FilterName");
            if (source == null || filterName == null || !Plugin.OBSWebsocket.IsConnected)
            {
                return new string[0];
            }

            List<string> optionals = new List<string>();

            try
            {
                var filter = Plugin.OBSWebsocket.GetSourceFilter(source, filterName);


                
                foreach (var item in filter.Settings)
                {
                    if (!propertys.ContainsKey(item.Key))
                    {
                        optionals.Add(item.Key);
                    }
                }

                JObject defaultSettings = Plugin.OBSWebsocket.GetSourceFilterDefaultSettings(filter.Kind)["defaultFilterSettings"].ToObject<JObject>();


                foreach (var item in defaultSettings)
                {
                    if (!propertys.ContainsKey(item.Key) && !optionals.Contains(item.Key))
                    {
                        optionals.Add(item.Key);
                    }
                }

                return optionals.ToArray();
            }
            catch
            {
                return optionals.ToArray();
            }
        }

        public override string[] GetPossibleValues(Dictionary<string, string> propertys, string property)
        {
            if (!Plugin.OBSWebsocket.IsConnected)
            {
                return null;
            }

            if (property == "FilterName")
            {
                String sourceName = this.GetSource(propertys, Plugin.PluginManager);

                if (sourceName == null)
                {
                    return null;
                }

                var list = Plugin.OBSWebsocket.GetSourceFilterList(sourceName);
                var result = new List<string>();
                foreach (var item in list)
                {
                    result.Add(item.Name);
                }

                return result.ToArray();
            }
            else if (property == "SourceName")
            {
                return base.GetPossibleValues(propertys, property);
            }
            else
            {
                try
                {
                    List<string> values = new List<string>();
                    var sourceName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "SourceName");
                    var filterName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, Plugin.PluginManager, "FilterName");

                    var filter = Plugin.OBSWebsocket.GetSourceFilter(sourceName, filterName);
                    if (filter.Settings.ContainsKey(property))
                    {
                        values.Add(filter.Settings[property].ToString());
                    }

                    var defaultSettings = Plugin.OBSWebsocket.GetSourceFilterDefaultSettings(filter.Kind)["defaultFilterSettings"].ToObject<JObject>();
                    if (defaultSettings.ContainsKey(property))
                    {
                        string val = defaultSettings[property].ToString();
                        if (!values.Contains(val))
                        {
                            values.Add(val);
                        }
                    }

                    return values.ToArray();
                }
                catch
                {
                    return null;
                }
            }

            
        }

        public override void MapAndRun(Dictionary<string, string> propertys, PluginManager pluginManager)
        {
            string source = this.GetSource(propertys, pluginManager);
            string filterName = SimHubOBSCustomActions.GetValueFromDictionary(propertys, pluginManager, "FilterName");

            var settings = new JObject();
            foreach (var property in propertys)
            {
                if (property.Key != "SourceName" && property.Key != "FilterName")
                {
                    JToken value = property.Value;
                    if (Int64.TryParse(property.Value, out long valInt))
                    {
                        value = valInt;
                    }
                    else if (Double.TryParse(property.Value, out double valDouble))
                    {
                        value = valDouble;
                    }
                    else if (Boolean.TryParse(property.Value, out bool valBoolean))
                    {
                        value = valBoolean;
                    }

                    settings.Add(property.Key, value);
                }
            }
            try
            {
                Plugin.OBSWebsocket.SetSourceFilterSettings(source, filterName, settings, overlay: true);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to change Filter Settings with a custom action: " + ex.Message);
            }
            
        }


    }
}
