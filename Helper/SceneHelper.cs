﻿using Lichten.SimHubHelper;
using OBSWebsocketDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public class SceneHelper : ListHelper
    {
        public OBSWebsocket Websocket { get; set; }

        private bool PreviewMode = false;

        public SceneHelper(OBSWebsocket websocket)
        {
            Websocket = websocket;
        }

        public SceneHelper(OBSWebsocket websocket, bool preview) : this(websocket)
        {
            PreviewMode = preview;
        }

        public override string GetCurrent()
        {
            if (PreviewMode)
                return Websocket.GetCurrentPreviewScene();

            return Websocket.GetCurrentProgramScene();
        }

        public override string[] GetList()
        {
            var list = Websocket.GetSceneList().Scenes;
            string[] arr = new string[list.Count];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[list.Count - 1 - i] = list[i].Name; //With 5.0 The scene list for some reason comes back inverted, so we return in the correct order
            }

            return arr;
        }
    }
}
