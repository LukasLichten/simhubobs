﻿using OBSWebsocketDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public class TransitionHelper : SceneHelper
    {
        public TransitionHelper(OBSWebsocket websocket) : base(websocket) { }

        public override string GetCurrent()
        {
            return Websocket.GetCurrentSceneTransition().Name;
        }

        public override string[] GetList()
        {
            var list = Websocket.GetSceneTransitionList().Transitions;
            string[] arr = new string[list.Count];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = list[i].Name;
            }

            return arr;
        }

        public int GetDuration()
        {
            return Websocket.GetCurrentSceneTransition().Duration is int val ? val : -1;
        }
    }
}
