﻿using Lichten.SimHubHelper;
using OBSWebsocketDotNet;
using SimHub.Plugins;
using SimHubOBS.Helper.CustomAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public class SimHubOBSCustomActions
    {
        public SimHubOBS Main { get; private set; }
        public Action UpdateUIActionList { get; set; }

        private Dictionary<string, ICustomActionFunction> CustomFunctions { get; set; }

        internal SimHubOBSCustomActions(SimHubOBS Main)
        {
            this.Main = Main;

            CustomFunctions = new Dictionary<string, ICustomActionFunction>
            {
                { "SetCurrentScene", new SetCurrentScene(Main) },
                { "SetCurrentProfile", new SetCurrentProfile(Main) },
                { "SetCurrentSceneCollection", new SetCurrentSceneCollection(Main) },
                { "SetPreviewScene", new SetPreviewScene(Main) },
                { "SetCurrentTransition", new SetCurrentTransition(Main) },
                { "SetCurrentTransitionDuration", new SetCurrentTransitionDuration(Main) },
                { "SourceStateEnable", new SetSceneItemVisibility(Main, SetSceneItemVisibility.Mode.Enable) },
                { "SourceStateDisable", new SetSceneItemVisibility(Main, SetSceneItemVisibility.Mode.Disable) },
                { "SourceStateToggle", new SetSceneItemVisibility(Main, SetSceneItemVisibility.Mode.Toggle) },
                { "SetSourceSettings", new SetSourceSettings(Main) },
                { "SourceFilterEnable", new SetSourceFilterState(Main, SetSourceFilterState.Mode.Enable) },
                { "SourceFilterDisable", new SetSourceFilterState(Main, SetSourceFilterState.Mode.Disable) },
                { "SourceFilterToggle", new SetSourceFilterState(Main, SetSourceFilterState.Mode.Toggle) },
                { "SetSourceFilterSettings", new SetSourceFilterSettings(Main) },
            };

            //Main.OBSWebsocket.SetSourceFilterSettings
            //Main.OBSWebsocket.GetSourceFilter
            //Main.OBSWebsocket.GetInputSettings
            //Main.OBSWebsocket.SetInputSettings

            RegisterCustomActions();
        }

        public void RegisterCustomActions()
        {
            Main.PluginManager.ClearActions(this.GetType()); // Removes old actions

            foreach (var item in Main.Settings.CustomActions)
            {
                if (CustomFunctions.ContainsKey(item.Function))
                    Main.PluginManager.AddAction(item.Name, this.GetType(), new CustomActionHelper(CustomFunctions[item.Function].MapAndRun, item.Propertys).TriggerAction);
            }

            if (UpdateUIActionList != null)
                UpdateUIActionList.Invoke();
        }

        public string[] GetFunctionsList()
        {
            List<string> list = new List<string>();

            foreach (var function in CustomFunctions)
            {
                list.Add(function.Key);
            }

            list.Sort();

            return list.ToArray();
        }

        public ICustomActionFunction GetFunction(string name)
        {
            if (name != null && CustomFunctions.ContainsKey(name))
            {
                return CustomFunctions[name];
            }

            return null;
        }

        public static string GetValueFromDictionary(Dictionary<string, string> propertys, PluginManager pluginManager, string key)
        {
            if (!propertys.ContainsKey(key))
            {
                return null;
            }

            string value = propertys[key];
            string propTest = value.Trim();
            if (propTest.ElementAtOrDefault(0) == '[' && propTest.ElementAtOrDefault(propTest.Length - 1) == ']')
            {
                string simHubProperty = propTest.Replace("[","").Replace("]","");
                value = pluginManager.GetPropertyValue(simHubProperty)?.ToString();
            }

            return value;
        }

        public static bool IsValueAPropertysName(PluginManager pluginManager, string value)
        {
            if (value == null)
                return false;

            string propTest = value.Trim();
            if (propTest.ElementAtOrDefault(0) == '[' && propTest.ElementAtOrDefault(propTest.Length - 1) == ']')
            {
                string simHubProperty = propTest.Replace("[", "").Replace("]", "");
                if (pluginManager.GetPropertyValue(simHubProperty) != null)
                {
                    return true;
                }
                
                // In case the value is null we need to check this list
                foreach (var item in pluginManager.GetAllPropertiesNames())
                {
                    if (item == simHubProperty)
                    {
                        return true;
                    }
                }

                // Some names in DataCore are shortened, so we need to check those manually
                // a lot are saved because they are never null, but these init at null
                string[] list = { "AccelerationHeave", "AccelerationSurge", "AccelerationSway", "BestSplitDelta", "CarClass",
                    "CarId", "CarModel", "DeltaToAllTimeBest", "DeltaToSessionBest", "PushToPassActive", "ReplayMode",
                    "Sector1BestLapTime", "Sector1BestTime", "Sector1LastLapTime", "Sector1Time",
                    "Sector2BestLapTime", "Sector2BestTime", "Sector2LastLapTime", "Sector2Time",
                    "Sector3BestLapTime", "Sector3BestTime", "Sector3LastLapTime", "Sector3Time",
                    "SectorsCount", "SelfsplitDelta", "SessionTypeName", "TrackCode", "TrackConfig", "TrackId", "TrackName", "Flag_Name"};
                return list.Contains(simHubProperty);
            }

            return false;
        }
    }
}
