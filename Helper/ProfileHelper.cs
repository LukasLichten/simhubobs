﻿using Lichten.SimHubHelper;
using OBSWebsocketDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS.Helper
{
    public class ProfileHelper : ListHelper
    {
        private OBSWebsocket Websocket { get; set; }

        public ProfileHelper(OBSWebsocket websocket)
        {
            Websocket = websocket;
        }

        public override string GetCurrent()
        {
            return Websocket.GetProfileList().CurrentProfileName;
        }

        public override string[] GetList()
        {
            var list = Websocket.GetProfileList().Profiles;
            string[] arr = new string[list.Count];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = list[i];
            }

            return arr;
        }
    }
}
