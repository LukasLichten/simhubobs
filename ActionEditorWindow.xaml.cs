﻿using SimHubOBS.Helper.UI;
using Lichten.SimHubHelper;
using SimHubOBS;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System;
using System.Xml.Linq;
using System.Windows.Forms;
using SimHub.Plugins.OutputPlugins.Dash.WPFUI;
using SimHub.Plugins.UI;

namespace SimHubOBS
{
    /// <summary>
    /// Interaction logic for PropertyEditWindow.xaml
    /// </summary>
    public partial class ActionEditorWindow : Window
    {
        private SimHubOBS Plugin;

        private ActionEditorModel Model;

        public ActionEditorWindow(SimHubOBS plugin)
        {
            InitializeComponent();

            Model = new ActionEditorModel() { Plugin = plugin };
            Init(plugin);
        }

        public ActionEditorWindow(SimHubOBS plugin, CustomActionContainer action)
        {
            InitializeComponent();

            Model = new ActionEditorModel() { Plugin = plugin, IsEdit = true, ActionContainer = action };
            Init(plugin);
        }

        internal void Init(SimHubOBS plugin)
        {
            Plugin = plugin;

            this.DataContext = Model;
        }

        private void BtnAgree_Click(object sender, RoutedEventArgs e)
        {
            CustomActionContainer actionContainer = new CustomActionContainer
            {
                Name = Model.Name.Trim(),
                Function = Model.Function,
                //Parameters = Model.ActionContainer?.Parameters,
                Propertys = Model.BuildPropertys()
            };

            //sanetize action name
            var listOfActions = Plugin.Settings.CustomActions;
            List<string> actionNames = new List<string>();
            foreach (var item in listOfActions)
            {
                if (item.Name != Model.ActionContainer?.Name)
                {
                    actionNames.Add(item.Name.ToLower()); //SimHub seems to treat Names for actions not case sensitive, having two functions with different case could lead to them overriding each other
                }
                else
                {
                    //This is the original name of this Action, so we don't need to do collision testing on it
                }
            }

            string nameCheck = actionContainer.Name.ToLower();
            foreach (var item in actionNames)
            {
                if (item == nameCheck)
                {
                    System.Windows.Forms.MessageBox.Show(
                        "Name Is already used by another CustomAction, please change it",
                        "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            if (!Model.GetFunctionList.Contains(Model.Function))
            {
                System.Windows.Forms.MessageBox.Show(
                        "Please Select A Valid Function",
                        "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // TODO Validate Propertys

            // Updating/Adding the action
            if (Model.IsEdit)
            {
                int index = Plugin.Settings.CustomActions.IndexOf(Model.ActionContainer);
                if (index != -1)
                    Plugin.Settings.CustomActions[index] = actionContainer;
                else
                    Plugin.Settings.CustomActions.Add(actionContainer); // This shoudl not happen, but just in case
            }
            else
            {
                Plugin.Settings.CustomActions.Add(actionContainer);
            }

            Plugin.CustomActionManager.RegisterCustomActions();
            Plugin.SaveSettings();
            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LvPropertys_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetSelectedIndex();
        }

        private ActionEditorModel.PropertyRow GetSelectedIndex()
        {
            object item = LvPropertys.SelectedItem;

            if (item is ActionEditorModel.PropertyRow row)
            {
                BtnDelProp.IsEnabled = row.NotRequired;
                return row;
            }
            else
            {
                BtnDelProp.IsEnabled = false;
            }

            return null;
        }

        private void BtnNewProp_Click(object sender, RoutedEventArgs e)
        {
            if (!Model.HasOptional)
            {
                Model.OnPropertyChanged(nameof(Model.HasOptional));
                return;
            }

            Model.Rows.Add(new ActionEditorModel.PropertyRow() { NotRequired = true});
        }

        private void BtnDelProp_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();
            if (item == null)
            {
                BtnDelProp.IsEnabled = false;
                return;
            }

            if (!item.NotRequired)
            {
                BtnDelProp.IsEnabled = false;
                return;
            }

            Model.Rows.Remove(item);
        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            if (sender is System.Windows.Controls.ComboBox combo)
            {
                if (combo.DataContext is ActionEditorModel.PropertyRow propertyRow)
                {
                    var func = Plugin.CustomActionManager.GetFunction(Model.Function);
                    propertyRow.PossibleValues = func?.GetPossibleValues(Model.BuildPropertys(), propertyRow.Name);
                    propertyRow.PossibleNames = func?.GetOptionalPropertys(Model.BuildPropertys());
                }

                if (combo.Items.Count == 0)
                {
                    combo.IsDropDownOpen = false;
                }
            }
        }

        private void PropertySelectorOpen_Click(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.Button button)
            {
                if (button.DataContext is ActionEditorModel.PropertyRow propertyRow)
                {
                    PropertiesPicker picker = new PropertiesPicker();
                    picker.ShowDialog(this, () => {
                        if (picker.Result == null)
                        {
                            return;
                        }
                        else
                        {
                            propertyRow.Value = '['+picker.Result.GetPropertyName()+']';
                        } 
                    }, DialogOptions.ForceNewWindow);
                }
            }
        }
    }
}
