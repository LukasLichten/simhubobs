Download this Plugin at RaceDepartment: https://www.racedepartment.com/downloads/simhub-obs-connector.46139/  
  
This Plugin uses SimHub: https://www.simhubdash.com/  
  
This Plugin allows you to get telemetry and control OBS via SimHub.  
You want to switch scenes depending on the Sim, start/stop recording through SimHubs mappings and controller input, save the replay buffer after setting a hotlap, monitor telemetry on your dash without need for an extra screen or more.  
It will automatically connect to OBS even if it is started after SimHub or runs on a seperate Computer.  
As with basically all of SimHub, the sky is the limit.  
  
Included are two demonstartion Dashboards, on is a small info Display, the other allows you to control OBS (perfect for a phone).  
  
## Changelog
### v2.1.0:
- Added New UI for creating/editing CustomActions
- Added that values for CustomActions can be sourced from Properties
- Added SetSourceSettings and SetSourceFilterSettings
- Added SourceFilterEnable/Disable/Toggle
- Added Icon to Plugin
- Fixed Softlock when creating/editing CustomActions
- Fixed ReplayBufferRunning causing an error on init
- Fixed ProfileListChanged and SceneCollectionChanged event not working

### Update v2.0:
- OBS 28+ now ships with the new websocket plugin, which is not compatible with v1.0 of this plugin. Also v2.0 is not compatible with OBS 27 and earlier.  
- Fixed Bugs and Major Optimizations  
- Renamed OutputFrames to EncoderFrames and added OutputFrames (which represent the total and dropped frames via network during stream)  
- Added Audio VolumeMeter  
- Added StreamTimecode  
- Added VirtualCamera Support  
- Added CustomActions for Turning Individual Sources on or off  
  
## To install the Plugin:  
Place the obs-websocket-dotnet.dll, Websocket.Client.dll, SimHubOBS.dll into your SimHub install.  
If you are updating from v1.0 of the plugin you will be promted to override obs-websocket-dotnet.dll and SimHubOBS.dll, just agree.  
websocket-sharp.dll is no longer in use and can be deleted.  
  
### Setup of OBS:  
1. Open OBS and then open the Websocket settings in the top toolbar under "Tools">"obs-websocket Settings".
2. Check "Enable WebSocket server"
3. The Windows Firewall may pop up to ask permission for OBS. You can deny this if you run SimHub and OBS on the same machine (or you can just confirm it like a regular user).  
4. "Show Connect Info" Button gives you an easy window to copy the password and other connection info.
  
### Setup SimHub:  
1. On start up of SimHub it will ask you to enable the Plugin.  
2. Open "Additional Plugins" (The Three dots in one row) and Select "SimHub OBS Connector" at the Top.  
3. Copy your Password into the Password field (If you Disabled Authentication then you leave this field empty)
4. Be aware if you update from v1.0: The new websocket now uses Port 4455 (instead of 4444), you can change this here or in OBS  
5. If you are running OBS on a seperate machine find out the Local-IP of the OBS machine then enter it here.  
6. The indicator should automatically turn green when OBS is running. If not, potential exceptions may be displayed to the right. It won't display errors if OBS is not running.  
7. (Optional) Install the two demonstration Dashboards by double clicking them.  
  
  
## Points of interest:  
- Properties including dropped frames, disk space, current scene, IsRecording, RecordingTimecode, etc.  
- Actions that you can map in Controls or Events such as Gamestart, actions ranging from start/stop recording, saving replay buffer, cyclingthrough scenes, muting audio, etc.
- Events to let OBS control SimHub whenever things like a recording start/stop, scene switches, etc.
- Custom Actions can be added through the Plugin Settings:
  - Change the Settings of a Filter/Source
  - Switch to a Specific Scene/Profile/etc.
  - Toggle a Specific Source on or off
  - Values can be sourced from SimHub Properties
  
  
## Further Info About SetSettings
See SourceAndFilterEditing.md
You don't need to pass all settings, only those you want to change

Known Issues/Limitations:
- Some Settings do not show up if they are default, but work when typing in the name manually or setting the value to something else
- Colors are strange integer values that can't be directly modified
- Display Capture target can be changed, but you have to use the optuse "monitor_id"
- Text Font and Size are passed in a JsonObject as font
- WindowSource/GameCapture window can be set, however the value is not convenient
- ImageSlideShow files are a JsonArray, updating it resets it to the first object, pause/skip/etc. do not work
  
  
If you have any suggestions you can reach me on RaceDeperatment via the link at the top or via Discord at @DerGeneralFluff  
  
  
Note for technical People interested in even more functionality:  
The websocket allows to completly remote control OBS, from creating, removing, filtering etc. I have not added these functions as a lot of them require specific parameters to be passed, while SimHub actions tend to be designed as parameterless functions.  
If you intend to use one of them with your personal parameters, you can make a SimHub plugin (SimHubSDK in your SimHub install), add a Dependency to the SimHubOBS.dll, in the SimHubOBS.SimHubOBS class you get through the static instance parameter the current instance of the plugin, through it you can directly access the OBSWebsocket without having to set up the UI and code to connect to the websocket.  