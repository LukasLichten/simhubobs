﻿using OBSWebsocketDotNet.Communication;
using System;
using System.Windows.Controls;

namespace SimHubOBS
{
    /// <summary>
    /// Logique d'interaction pour SettingsControlDemo.xaml
    /// </summary>
    public partial class ShObsSettingsControl : UserControl
    {
        public SimHubOBS Plugin { private set; get; }

        public ShObsSettingsControl(SimHubOBS plugin)
        {
            this.Plugin = plugin;
            InitializeComponent();

            Plugin.OBSWebsocket.Connected += OBSWebsocket_Connected;
            Plugin.OBSWebsocket.Disconnected += OBSWebsocket_DisConnected;
            Plugin.OBSWebsocket.ExitStarted += OBSWebsocket_Connected;

            Plugin.UpdateUIAlertBox = ExternalAlertBoxUpdate;
            Plugin.CustomActionManager.UpdateUIActionList = ExternalCustomActionListUpdate;

            LoadSettings();

            ReloadActions();
            LbCustomActions.SelectionChanged += LbCustomActions_SelectionChanged;
        }

        private void LoadSettings()
        {
            TbIPAddress.Text = Plugin.Settings.IpAdresse;
            TbPort.Text = Plugin.Settings.Port +  "";
            TbPassword.Password = Plugin.Settings.Password;
            SetSkipUpdate(Plugin.Settings.UpdateRateOffset);
            ChkMultiThread.IsChecked = Plugin.Settings.MultiThreaded;

            BtnApply.Content = "Loaded";
            BtnApply.IsEnabled = false;
            BtnRevert.IsEnabled = false;

            UpdateConnectionLight();
        }

        private void UpdateConnectionLight()
        {
            UpdateConnectionLight(false);
        }

        private void UpdateConnectionLight(bool forceOff)
        {
            BtnConnect.ToolTip = "";

            if (Plugin.OBSWebsocket.IsConnected && !forceOff)
            {
                LightConnection.Fill = System.Windows.Media.Brushes.Green;
                BtnConnect.Content = "Disconnect";
                LblAlertBox.Text = "";
            }
            else if (Plugin.SkipCounter < -1000)
            {
                LightConnection.Fill = System.Windows.Media.Brushes.Red;
                BtnConnect.Content = "Connect (M)";
                BtnConnect.ToolTip = "Automating Connecting has been suspended, Press this to resume it again";
            }
            else
            {
                LightConnection.Fill = System.Windows.Media.Brushes.Red;
                BtnConnect.Content = "Connect";
            }
        }

        private void SetSkipUpdate(int length)
        {
            SlSkipUpdate.Value = length;
            TbSkipUpdate.Text = "" + length;

            Tweaks();
        }

        private void Tweaks()
        {
            TbPort.Foreground = System.Windows.Media.Brushes.Black;
            TbSkipUpdate.Foreground = System.Windows.Media.Brushes.Black;
            BtnRevert.IsEnabled = true;

            //Blocking Apply being available when a number field is not a number 
            int iter = 0; //This is used to figure out which field threw the error to color the correct one
            try
            {
                Int32.Parse(TbPort.Text);
                iter = 1;
                Int32.Parse(TbSkipUpdate.Text);
                iter = 2;
            }
            catch
            {
                BtnApply.IsEnabled = false;

                switch (iter)
                {
                    case 0:
                        TbPort.Foreground = System.Windows.Media.Brushes.Red;
                        break;
                    case 1:
                        TbSkipUpdate.Foreground = System.Windows.Media.Brushes.Red;
                        break;
                    default:
                        break;
                }

                
                return;
            }

            BtnApply.IsEnabled = true;
            BtnApply.Content = "Apply";
        }

        private void BtnApply_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var settings = Plugin.Settings;

            bool requiresRefresh = settings.IpAdresse != TbIPAddress.Text || settings.Password != TbPassword.Password;

            try
            {
                int port = Int32.Parse(TbPort.Text);

                requiresRefresh = requiresRefresh || settings.Port != port;


                settings.Port = port;
                settings.UpdateRateOffset = Int32.Parse(TbSkipUpdate.Text);

            }
            catch
            {
                LblAlertBox.Text = "Write valid numbers into the text boxes";
                return;
            }

            settings.IpAdresse = TbIPAddress.Text;
            settings.Password = TbPassword.Password;
            settings.MultiThreaded = ChkMultiThread.IsChecked.Value;

            LblAlertBox.Text = "";
            BtnApply.Content = "Applied";
            BtnApply.IsEnabled = false;
            BtnRevert.IsEnabled = false;

            if (requiresRefresh) //The Websocket has to be disconnected, so the Update loop reconnects it with new valid data
            {
                Plugin.OBSWebsocket.Disconnect();
            }

            Plugin.SkipCounter = Plugin.Settings.UpdateRateOffset; //Forces to run an update immediatly
            Plugin.SaveSettings();
        }

        private void BtnRevert_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadSettings();
        }

        private void ChkMultiThread_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            Tweaks();
        }

        private void TbIPAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            Tweaks();
        }

        private void TbPort_TextChanged(object sender, TextChangedEventArgs e)
        {
            Tweaks();
        }

        private void TbPassword_TextChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            Tweaks();
        }

        private void SlSkipUpdate_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            int value = (int) SlSkipUpdate.Value;
            SetSkipUpdate(value);
        }

        private void TbSkipUpdate_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TbSkipUpdate.Text;
            try
            {
                int value = Int32.Parse(text);
                SetSkipUpdate(value);
            }
            catch
            {
                Tweaks();
            }
        }

        private void OBSWebsocket_Connected(object sender, EventArgs e)
        {
            LightConnection.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(this.UpdateConnectionLight));
        }

        private void OBSWebsocket_DisConnected(object sender, ObsDisconnectionInfo e)
        {
            LightConnection.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(disable));

            void disable()
            {
                this.UpdateConnectionLight(true);
            }
        }

        private void ExternalAlertBoxUpdate(string text)
        {
            LblAlertBox.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() => LblAlertBox.Text = text));
        }

        private void ExternalCustomActionListUpdate()
        {
            LbCustomActions.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() => ReloadActions()));
        }

        private void BtnConnect_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            LblAlertBox.Text = "";

            if (Plugin.OBSWebsocket.IsConnected)
            {
                //Disconnect
                Plugin.SkipCounter = Int32.MinValue; //This will cause it to skip the update for the next 414 days, so it can not reconnect

                Plugin.OBSWebsocket.Disconnect();
            }
            else
            {
                //Force Connect
                Plugin.SkipCounter = Plugin.Settings.UpdateRateOffset; //This will cause to on the next simhub update to run an update, and then connect (if not throwing an error)
            }
        }

        private void ReloadActions()
        {
            LbCustomActions.Items.Clear();

            foreach (var item in Plugin.Settings.CustomActions)
            {
                LbCustomActions.Items.Add(item.Name);
            }

            LbCustomActions_SelectionChanged(null, null);
        }

        private void LbCustomActions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool enabledEdit = LbCustomActions.SelectedIndex != -1;

            BtnCustomEdit.IsEnabled = enabledEdit;
            BtnCustomDel.IsEnabled = enabledEdit;
        }

        private void BtnCustomNew_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ActionEditorWindow editor = new ActionEditorWindow(Plugin);
            editor.Show();
            //CustomActionEditor editor = new CustomActionEditor(Plugin);
        }

        private CustomActionContainer GetActionSelected()
        {
            if (LbCustomActions.SelectedItem == null)
            {
                System.Windows.Forms.MessageBox.Show("You can't edit or delete and Action without selecting it", "Please Action", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                return null;
            }

            return Plugin.Settings.CustomActions[LbCustomActions.SelectedIndex];
        }

        private void BtnCustomEdit_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            CustomActionContainer action = GetActionSelected();

            if (action == null)
                return;

            ActionEditorWindow editor = new ActionEditorWindow(Plugin, action);
            editor.Show();
            //CustomActionEditor editor = new CustomActionEditor(Plugin, action);
        }

        private void BtnCustomDel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            CustomActionContainer action = GetActionSelected();

            if (action == null)
                return;

            var result = System.Windows.Forms.MessageBox.Show("Do you want to Delete \""+action.Name+"\"? \nThe plugin needs to be reloaded (switching games, restarting simhub) prior to it disappearing out of Action lists", "Comfirm Deletion", System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Warning);

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Plugin.Settings.CustomActions.Remove(action);
                ReloadActions();
                //Plugin.CustomActionManager.RegisterCustomActions(); //Not really needed as there are no new Actions
            }
        }
    }
}
