﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimHubOBS
{
    public class CustomActionContainer
    {
        public string Name { get; set; }
        public string Function { get; set; }

        public Dictionary<string, string> Propertys { get; set; }

        [Obsolete]
        public object[] Parameters {
            set
            {
                if (value != null && value.Length > 0 && Propertys == null )
                {
                    // Migration of old Parameter system to new Properties
                    string val = value[0].ToString();

                    string key = "Property";
                    switch (Function)
                    {
                        case "SetPreviewScene":
                        case "SetCurrentScene":
                            key = "SceneName";
                            break;
                        case "SetCurrentProfile":
                            key = "ProfileName";
                            break;
                        case "SetCurrentSceneCollection":
                            key = "SceneCollectionName";
                            break;
                        case "SetCurrentTransition":
                            key = "TransitionName";
                            break;
                        case "SetCurrentTransitionDuration":
                            key = "TransitionDuration";
                            break;
                        case "SourceEnable":
                        case "SourceDisable":
                        case "SourceToggle":
                            key = "SourceName";
                            break;

                    }

                    Propertys = new Dictionary<string, string>()
                    {
                        { key, val }
                    };
                }
            }
        }
        
    }
}
